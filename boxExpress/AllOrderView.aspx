﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllOrderView.aspx.cs" Inherits="boxExpress.AllOrderView" %>

<!DOCTYPE html>
<!--
Item Name: Elisyam - Web App & Admin Dashboard Template
Version: 1.5
Author: SAEROX

** A license must be purchased in order to legally use this template for your project **
-->
<html lang="en" runat="server">
    <head>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css/">
        <link href="Content/Site.css" rel="stylesheet" />
        <link href="assets/main.css" rel="stylesheet" />
        <link href="assets/util.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Elisyam - Login</title>
        <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
            });
        </script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
        <!-- Stylesheet -->
        <link rel="stylesheet" href="assets/vendors/css/base/bootstrap.min.css">
        <link rel="stylesheet" href="assets/vendors/css/base/elisyam-1.5.min.css">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body>
        <!-- Begin Preloader -->
        <div id="preloader" style="background-color: #0d0d0d;">
            <div class="canvas">
                <img src="assets/img/logo.png" alt="logo" class="loader-logo">
                <div class="spinner"></div>   
            </div>
        </div>
        <!-- End Preloader -->
        <!-- Begin Container -->
        <div class="container-fluid no-padding h-100" style="background-color:#0d0d0d;">
            <div class="container no-padding h-100">
                <!-- Begin Left Content -->
               
                <!-- End Left Content -->
                <!-- Begin Right Content -->
                <div class="row h-100 justify-content-center align-items-center">
                    <!-- Begin Form -->
                    <div class="authentication-form mx-auto">
                        <div class="logo-centered">
                            <asp:ListView ID="OrderListview" runat="server" DataKeyNames="UserID">
                            <LayoutTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <table runat="server" border="1">
                                                <tr>
                                                    <th runat="server">User ID
                                                    </th>
                                                    <th runat="server">Name
                                                    </th>
                                                    <th runat="server">Price
                                                    </th>
                                                    <th runat="server">Pickup Place
                                                    </th>
                                                    <th runat="server">Derivery Place
                                                    </th>
                                                    <th runat="server">Lenght
                                                    </th>
                                                </tr>
                                                <tr id="itemPlaceholder" runat="server">
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>

                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("UserID") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("price") %>'  />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPickup" runat="server" Text='<%# Eval("Pickup") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDerivery" runat="server" Text='<%# Eval("Derivery") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLenght" runat="server" Text='<%# Eval("Lenght") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td colspan="6">
                                        <hr>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                        </div>
                        <hr style="border-color:white" />
                        
                        <form id="OrderShow" runat="server" /> 
                        
                    </div>
                    <!-- End Form -->                        
                </div>
                <!-- End Right Content -->
            </div>
            <!-- End Row -->
        </div>
        <!-- End Container -->    
        <!-- Begin Vendor Js -->
        <script src="assets/vendors/js/base/jquery.min.js"></script>
        <script src="assets/vendors/js/base/core.min.js"></script>
        <!-- End Vendor Js -->
        <!-- Begin Page Vendor Js -->
        <script src="assets/vendors/js/app/app.min.js"></script>
        <!-- End Page Vendor Js -->
        <script src="Scripts/main.js"></script>
        <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
        <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    </body>
</html>