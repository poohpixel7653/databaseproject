﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;

namespace boxExpress
{
    public partial class AllOrderView : System.Web.UI.Page
    {

        string CS = ConfigurationManager.ConnectionStrings["FinalProjectConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateGridview();
        }

        void PopulateGridview()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(CS))
            {
                sqlCon.Open();
                string strSql = "select PDUser.UserID,PDUser.Name,PDOrder.price,PDOrder.Date,PDOrder.Pickup,PDOrder.Derivery,PDOrder.Lenght from PDUser join PDOrder on PDUser.UserID = PDOrder.UserID";
                SqlDataAdapter da = new SqlDataAdapter(strSql,sqlCon);
                da.Fill(dtbl);

                OrderListview.DataSource = dtbl;
                OrderListview.DataBind();

            }
        }

    }
}
