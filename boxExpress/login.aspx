﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="boxExpress.login" %>

<!DOCTYPE html>
<!--
Item Name: Elisyam - Web App & Admin Dashboard Template
Version: 1.5
Author: SAEROX

** A license must be purchased in order to legally use this template for your project **
-->
<html lang="en" runat="server">
    <head>
        <link href="assets/main.css" rel="stylesheet" />
        <link href="assets/util.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Elisyam - Login</title>
        <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
        <!-- Stylesheet -->
        <link rel="stylesheet" href="assets/vendors/css/base/bootstrap.min.css">
        <link rel="stylesheet" href="assets/vendors/css/base/elisyam-1.5.min.css">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body>
        <!-- Begin Preloader -->
        <div id="preloader" style="background-color: #0d0d0d;">
            <div class="canvas">
                <img src="assets/img/logo.png" alt="logo" class="loader-logo">
                <div class="spinner"></div>   
            </div>
        </div>
        <!-- End Preloader -->
        <!-- Begin Container -->
        <div class="container-fluid no-padding h-100" style="background-color:#0d0d0d;">
            <div class="container no-padding h-100">
                <!-- Begin Left Content -->
               
                <!-- End Left Content -->
                <!-- Begin Right Content -->
                <div class="row h-100 justify-content-center align-items-center">
                    <!-- Begin Form -->
                    <div class="authentication-form mx-auto">
                        <div class="logo-centered">
                            <a href="/">
                                <img src="assets/img/logo.png" alt="logo">
                            </a>
                        </div>
                        <form id="login" runat="server">
                            <label style="color:white;">Username</label>
                            <div class="group material-input" runat="server">
                                <asp:TextBox ID="txtUsername" runat="server" Class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorUsername" CssClass="text-danger" runat="server" ErrorMessage="The Username field is Required !" ControlToValidate="txtUsername"></asp:RequiredFieldValidator>
							    <span class="highlight"></span>
							    <span class="bar"></span>
                            </div>
                            <label style="color:white;">Password</label>
                            <div class="group material-input">
                                <asp:TextBox ID="txtPassword" runat="server" Class="form-control" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" CssClass="text-danger" runat="server" ErrorMessage="The Password field is Required !" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
							    <span class="highlight"></span>
							    <span class="bar"></span>
                            </div>
                            <div class="row">
                                <div class="col text-left">
                                    <div class="styled-checkbox">
                                        <input type="checkbox" name="checkbox" id="remeber">
                                        <label for="remeber">Remember me</label>
                                    </div>
                                </div>
                                <div class="col text-right" style="left: 0px; top: 0px">
                                    <a href="/forget">Forgot Password ?</a>
                                </div>
                            </div>
                            <div class="sign-btn text-center">
                                <asp:Button ID="Button1" runat="server" Text="Sign in" class="btn btn-lg btn-gradient-01"/>
                            </div>

                        </form>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="wrong" runat="server" visible="false">
                            please login
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          </button>
                        </div>
                        
                            
                        <div class="txt1 text-center p-t-54 p-b-20">
						<span>
							Or Sign Up Using
						</span>
					</div>

					<div class="flex-c-m">
						<a href="#" class="login100-social-item bg1">
							<i class="fa fa-facebook"></i>
						</a>

						<a href="#" class="login100-social-item bg2">
							<i class="fa fa-twitter"></i>
						</a>

						<a href="#" class="login100-social-item bg3">
							<i class="fa fa-google"></i>
						</a>
					</div>
                        <div class="register">
                            Don't have an account? 
                            <br>
                            <a href="signup">Create an account</a>
                        </div>
                    </div>
                    <!-- End Form -->                        
                </div>
                <!-- End Right Content -->
            </div>
            <!-- End Row -->
            </div>
        <!-- End Container -->    
        <!-- Begin Vendor Js -->
        <script src="assets/vendors/js/base/jquery.min.js"></script>
        <script src="assets/vendors/js/base/core.min.js"></script>
        <!-- End Vendor Js -->
        <!-- Begin Page Vendor Js -->
        <script src="assets/vendors/js/app/app.min.js"></script>
        <!-- End Page Vendor Js -->
    </body>
</html>