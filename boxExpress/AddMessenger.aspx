﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddMessenger.aspx.cs" Inherits="boxExpress.AddMessenger" %>

<!DOCTYPE html>
<!--
Item Name: Elisyam - Web App & Admin Dashboard Template
Version: 1.5
Author: SAEROX

** A license must be purchased in order to legally use this template for your project **
-->
<html lang="en" runat="server">
    <head>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css/">
        <link href="Content/Site.css" rel="stylesheet" />
        <link href="assets/main.css" rel="stylesheet" />
        <link href="assets/util.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Elisyam - Login</title>
        <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
            });
        </script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
        <!-- Stylesheet -->
        <link rel="stylesheet" href="assets/vendors/css/base/bootstrap.min.css">
        <link rel="stylesheet" href="assets/vendors/css/base/elisyam-1.5.min.css">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body>
        <!-- Begin Preloader -->
        <div id="preloader" style="background-color: #0d0d0d;">
            <div class="canvas">
                <img src="assets/img/logo.png" alt="logo" class="loader-logo">
                <div class="spinner"></div>   
            </div>
        </div>
        <!-- End Preloader -->
        <!-- Begin Container -->
        <div class="container-fluid no-padding h-100" style="background-color:#0d0d0d;">
            <div class="container no-padding h-100">
                <!-- Begin Left Content -->
               
                <!-- End Left Content -->
                <!-- Begin Right Content -->
                <div class="row h-100 justify-content-center align-items-center">
                    <!-- Begin Form -->
                    <div class="authentication-form mx-auto">
                        <div class="logo-centered">
                            <a href="/">
                                <img src="assets/img/logo.png" alt="logo">
                            </a>
                        </div>
                        <hr style="border-color:white" />
                        <form id="rergister" runat="server"> 
                            <div class="row">
                                <div class="col">
                                    <label style="color:white;">Username</label>
                                        <div class="group material-input" runat="server">
                                            <asp:TextBox ID="username" runat="server" Class="form-control"></asp:TextBox>
							                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="text-danger" runat="server" ErrorMessage="The Username field is Required !" ControlToValidate="username"></asp:RequiredFieldValidator>
                                            <span class="highlight"></span>
							                <span class="bar"></span>
                                        </div> 
                                 </div>
                                <div class="col">
                                    <label style="color:white;">Email</label>
                                    <div class="group material-input">
                                        <asp:TextBox ID="Email" runat="server" Class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="text-danger" runat="server" ErrorMessage="The Email field is Required !" ControlToValidate="Email"></asp:RequiredFieldValidator>
							            <span class="highlight"></span>
							            <span class="bar"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                <label style="color:white;">Password</label>
                                    <div class="group material-input">
                                        <asp:TextBox ID="newpassword" runat="server" Class="form-control" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" CssClass="text-danger" runat="server" ErrorMessage="The Password field is Required !" ControlToValidate="newpassword"></asp:RequiredFieldValidator>
							            <span class="highlight"></span>
							            <span class="bar"></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <label style="color:white;">Confirm Password</label>
                                    <div class="group material-input">
                                        <asp:TextBox ID="confirmpassword" runat="server" Class="form-control" TextMode="Password"></asp:TextBox>
							            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="text-danger" runat="server" ErrorMessage="The Confirmed Password field is Required !" ControlToValidate="confirmpassword"></asp:RequiredFieldValidator>
                                        <span class="highlight"></span>
							            <span class="bar"></span>
                                    </div>
                                    </div>
                            </div>
                           
                            <div class="row">
                                <div class="col-sm-4">
                                    <label style="color:white;">Name</label>
                                    <div class="group material-input">
                                        <asp:TextBox ID="name" runat="server" Class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" CssClass="text-danger" runat="server" ErrorMessage="The Name field is Required !" ControlToValidate="name"></asp:RequiredFieldValidator>
							            <span class="highlight"></span>
							            <span class="bar"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                        <label style="color:white;">Date Of Birth</label>
                                        <div class="group material-input">
                                            <asp:TextBox type="text" runat="server"  id="datepicker" class="form-control" placeholder="Choose"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" CssClass="text-danger" runat="server" ErrorMessage="The Date field is Required !" ControlToValidate="datepicker"></asp:RequiredFieldValidator>
							                <span class="highlight"></span>
							                <span class="bar"></span>				    
                                        </div>
                                    </div>
                                <div class="col-sm-4">
                                     <label style="color:white;">Sex</label>
                                        <div class="group material-input">
                                            <asp:DropDownList ID="ddlSex" runat="server" CssClass="form-control" >
                                                <asp:ListItem>Male</asp:ListItem>
                                                <asp:ListItem>Female</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="text-danger" runat="server" ErrorMessage="The Sex field is Required !" ControlToValidate="ddlSex"></asp:RequiredFieldValidator>
							                <span class="highlight"></span>
							                <span class="bar"></span>
                                        </div>
                                </div>
                            </div>

                           
                            <label style="color:white;">Phone number</label>
                            <div class="group material-input">
                                <asp:TextBox ID="phone" runat="server" Class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" CssClass="text-danger" runat="server" ErrorMessage="The Phone field is Required !" ControlToValidate="phone"></asp:RequiredFieldValidator>
							    <span class="highlight"></span>
							    <span class="bar"></span>
                            </div>

                            <label style="color:white;">Address</label>
                            <div class="group material-input">
                                <asp:TextBox ID="address" runat="server" Class="form-control" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" CssClass="text-danger" runat="server" ErrorMessage="The Address field is Required !" ControlToValidate="address"></asp:RequiredFieldValidator>
							    <span class="highlight"></span>
							    <span class="bar"></span>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="group material-input">
                                        <div class="input-group">
                                          <asp:FileUpload ID="imgInp" class="form-control; btn btn-default btn-file" runat="server"></asp:FileUpload>
                                           </span>
                                    </div>
                                       
                                </div>
                                    </div>
                                <div class="col-sm-4  text-center">
                                         <img src="https://bemynt.com/wp-content/uploads/2017/06/wood-main-dark-bg.jpg" id="img-upload" class="item4"/>
                                        <asp:Label ID="txtCheck" runat="server" Text=""></asp:Label>
							            <span class="highlight"></span>
							            <span class="bar"></span>
                                </div>
                                <div class="col-sm-4">
                                    <div class="sign-btn text-center">
                                        <asp:Button ID="Button1" OnClick="btSignUp_OnClick" runat="server" Text="Sign Up" class="btn btn-lg btn-gradient-01"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="register">
                            Do you have account? 
                            <br>
                            <a href="login">Sign In</a>
                        </div>
                    </div>
                    <!-- End Form -->                        
                </div>
                <!-- End Right Content -->
            </div>
            <!-- End Row -->
        </div>
        <!-- End Container -->    
        <!-- Begin Vendor Js -->
        <script src="assets/vendors/js/base/jquery.min.js"></script>
        <script src="assets/vendors/js/base/core.min.js"></script>
        <!-- End Vendor Js -->
        <!-- Begin Page Vendor Js -->
        <script src="assets/vendors/js/app/app.min.js"></script>
        <!-- End Page Vendor Js -->
        <script src="Scripts/main.js"></script>
        <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
        <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    </body>
</html>
