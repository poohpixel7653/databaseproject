﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.IO;

namespace boxExpress
{
    public partial class signup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btSignUp_OnClick(object sender, EventArgs e)
        {
            if (username.Text != "" & newpassword.Text != "" && name.Text != "" && Email.Text != "" && confirmpassword.Text != "")
            {
                if (newpassword.Text == confirmpassword.Text)
                {
                    DatabaseDataContext db = new DatabaseDataContext();
                    Auth dbAuth = new Auth();
                    PDUser dbUser = new PDUser();
                    dbAuth.UserID = username.Text;
                    dbAuth.Password = newpassword.Text;
                    dbAuth.Email = Email.Text;
                    dbAuth.type = "User";

                    dbUser.UserID = username.Text;
                    dbUser.Name = name.Text;
                    dbUser.BirthDate = datepicker.Text;
                    dbUser.Sex = ddlSex.Text;
                    dbUser.PhoneNum = phone.Text;
                    dbUser.Address = address.Text;
                    dbUser.RegDate = ((DateTime.Now.Date).ToString() + " | " + (DateTime.Now.Hour).ToString() + ":" + (DateTime.Now.Minute).ToString() + ":" + (DateTime.Now.Second).ToString());
                    db.Auths.InsertOnSubmit(dbAuth);
                    db.PDUsers.InsertOnSubmit(dbUser);
                    db.SubmitChanges();


                }
                else
                {
                    //lblMsg.ForeColor = Color.Red;
                    //lblMsg.Text = "Passwords do not match";
                }
            }
            else
            {
                //lblMsg.ForeColor = Color.Red;
                //lblMsg.Text = "All Fields Are Mandatory";

            }
        }
    }
}